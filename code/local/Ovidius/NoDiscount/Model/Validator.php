<?php
class Ovidius_NoDiscount_Model_Validator extends Mage_SalesRule_Model_Validator
{
    public function process(Mage_Sales_Model_Quote_Item_Abstract $item)
    {
        $product = $item->getProduct();
        // Determine if product is discounted
        $isDiscounted = (float)$product->getPrice() != (float)$product->getFinalPrice();

        if (!$isDiscounted) {
            // item is not discounted -> apply rules as usual
            return parent::process($item);
        }
        else {
            // item is discounted -> return without applying rules
            return $this;
        }
     }
}
