This module excludes discounted products from promotion rules.

It's done by overriding SalesRule_Model_Validator process method.

Tested on Magento versions: 1.9.3.1
